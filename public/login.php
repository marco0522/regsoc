<?php 
	session_start();
	if(isset($_SESSION['user_id'])){
		header('Location: index.php');
	}
		include_once 'header.php'; 
?>


	<form action="user_session.php" method="post">
		<div class="row">
			
				<fieldset class="small-6 columns small-centered">
			    	<legend>登入</legend>

			    	<label>登入帳號
			    	  <input type="text" name="user_id">
			    	</label>
			    	<label>密碼
			    	  <input type="password" name="password">
			    	</label>
					<div class="row">
				    	<div class="small-6 columns"><input type="submit" value="登入" class="button tiny"></div>
						<div class="small-6 columns"><a href="#" class="button secondary tiny right">索取屬會帳號</a><div>
					</div>
			 	</fieldset>
		 	
	 	</div>
	</form>


<?php include_once 'footer.php'; ?>
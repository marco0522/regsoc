<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="zh-tw" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>屬下團體週年登記網上系統</title>
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="css/app.css">
  <script src="js/vendor/modernizr.js"></script>

</head>
<body>
<div class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap">

    <!-- Off Canvas Menu -->
    <aside class="left-off-canvas-menu">
        <!-- whatever you want goes here -->
       	<?php 
       		if($_SESSION['user_id']==""){
					include_once 'left_nav.php';
				}else if($_SESSION['user_id']=="ADMIN"){
					include_once 'admin/left_nav.php';
				}else{
					include_once 'left_nav.php';
				}
       	 ?>
    </aside>

	<header>
		<nav class="top-bar" data-topbar role="navigation">

		  
		<section class="left-small tab-bar" style="border:none">
		  	<a class="left-off-canvas-toggle menu-icon" style="width:34em;">
		 <span>中大學生會屬下團體週年登記網上系統</span>
		  	</a>
		  </section>

 			<ul class="title-area" >
		    <li class="name" >
		      <h1><a href="index.php"></a></h1>
		    </li>
		     
		     
		  </ul>
		
		  <section class="top-bar-section">
		    <!-- Right Nav Section -->
		    <ul class="right">
		      <li><a href="notice.php">公告</a></li>
		      <li class="active">
		      	
		     <?php
		      	echo isset($_SESSION['user_id'] ) ?  '<a href="logout.php">登出</a>' : '<a href="login.php">登入</a>';

		      	?>

		      </li>
		    </ul>

		  </section>
		</nav>
	</header>

  <!-- body content here -->
  
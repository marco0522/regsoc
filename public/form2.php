<?php
session_start();
require_once 'user_session.php';


// if(!empty($_SESSION['user_id'])){
// 		$accountQuery = $db->prepare("SELECT * FROM society where soc_code = :soc_code");
// 		$accountQuery->execute([ 'soc_code' => $_SESSION['user_id'] ] );
// 		$account = $accountQuery->fetchAll();
// 		//$account = ($accountQuery->rowCount() == 1) ? $accountQuery : '';
		

// 		}
require_once 'header.php';

?>
<br>
<div class="row">
	<div class="medium-12 columns">
		<h1>表格 2 - 幹事會資料</h1>
		<form action="submit_form2.php" method="post">
			<labal>幹事會內閣名稱
				<input type="text" value="<?php echo $account[0]['name']; ?>">
				任期
				<input type="date">
			</labal>
			幹事會成員 <a id="addPost" class="button tiny right round" onclick="addPost()"> + 增加幹事 </a>
			<div style="display:none">

				<fieldset class="post" id="postInfoTemp" >
				    	<legend>職位
						<input type="text" name="post[]"></legend>
				<div class="row">
					<div class="columns medium-4">
						中文姓名
						<input type="text" name="chineseName[]">
					</div>

					<div class="columns medium-4" name="englishName[]">
						英文姓名
						<input type="text">
					</div>

					<div class="columns medium-4">
						學生證編號
						<input type="text" name="sid[]">
					</div>
				</div>
				<div class="row">
					<div class="columns medium-4">
						聯絡電話
						<input type="text" name="phone[]">
					</div>
					<div class="columns medium-3">
						書院
						<select name="college[]" id="">
							<option value="CC">崇基學院</option>
							<option value="NA">新亞書院</option>
							<option value="UC">聯合書院</option>
							<option value="SC">逸夫書院</option>
							<option value="SH">晨興書院</option>
							<option value="MC">善衡書院</option>
							<option value="WS">和聲書院</option>
							<option value="WYS">伍宜孫書院</option>
							<option value="CW">敬文書院</option>
						</select>
					</div>
					<div class="columns medium-3">
						學系
						<input type="text" name="dept[]">
					</div>
					<div class="columns medium-2">
						年級
						<input type="text" name="year[]">
					</div>

				</div>
				</fieldset>	
			</div>
			<div id="postInfo">
				
			</div>
	

			
			<input type="submit" value="遞交" class="button">
		</form>
		
	</div>
</div>

<script>

	function addPost(){
		var itm=document.getElementById("postInfoTemp");
		var cln=itm.cloneNode(true);
		document.getElementById("postInfo").appendChild(cln);
	}
</script>
<?php 
require_once 'footer.php';
?>
<?php
session_start();
require_once 'user_session.php';


// if(!empty($_SESSION['user_id'])){
// 		$accountQuery = $db->prepare("SELECT * FROM society where soc_code = :soc_code");
// 		$accountQuery->execute([ 'soc_code' => $_SESSION['user_id'] ] );
// 		$account = $accountQuery->fetchAll();
// 		//$account = ($accountQuery->rowCount() == 1) ? $accountQuery : '';
		

// 		}
require_once 'header.php';

?>
<br>
<div class="row">

	<div class="medium-12 columns">
		<h1>屬下團體週年登記</h1>
		<table>	
			<tr> 
				<th width="100">編號</th>
				<th width="200">名稱</th>
				<th width="100">登記狀態</th>
				<th width="300"></th>
			</tr>
			<tr> <th colspan="4" style="background-color:#333;color:#FFF;">工程學院</th> </tr>
			<tr> 
				<td>NS-CSC</td>
				<td><a href="/regsoc1">計算機科學系會</a></td>
				<td style="background-color: #a0d3e8;">處理中</td>
				<td><a href="#">屬下團體資訊</a></td>
			</tr>
			<tr> 
				<td>NS-ERG</td>
				<td><a href="/regsoc1">工程學院院會</td>
				<td style="background-color: #43AC6A; color:#FFF">已完成</td>
				<td>
					<a href="#">屬下團體資訊</a>｜ 
					<a href="#">宿分資料</a> ｜ 
					<a href="#">列印證明信</a>
				</td>
			</tr>
			<tr> <th colspan="4" style="background-color:#333;color:#FFF;">理學院</th> </tr>
			<tr> 
				<td>SS-PHY</td>
				<td><a href="/regsoc1">物理系系會</td>
				<td style="background-color: #f08a24;color:#FFF">未登記</td>
				<td> <a href="#">屬下團體資訊</a></td>
			</tr>
			<tr> <th colspan="4" style="background-color:#333;color:#FFF;">興趣學會</th> </tr>
			<tr> 
				<td>HC-FOD</td>
				<td><a href="/regsoc1">摺紙學會</a></td>
				<td style="background-color: #f04124;color:#FFF;">拒絕</td>
				<td><a href="#">屬下團體資訊</a> </td>
			</tr>
		</table>
	</div>
</div>

<?php 
require_once 'footer.php';
?>
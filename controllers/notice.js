var mysql 	= require('../db');
var Notice 	= require('../models/notice');
var User 	= require('../models/user');
var CheckLogin  = require('../models/checkLogin');


exports.getPost = function(req, res){
		if(req.query.page==undefined || req.query.page < 1){
			return res.redirect('/notice?page=1');
		}
		User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		Notice.index(req,res, mysql, function(list){ 
			Notice.getNotice(
				req, res, mysql, req.query.page, list,
				function(obj){
					res.render( 'notice',{
						notice : obj,
						currentIndex : req.query.page,
						pageNav : list.length,
						noticelist: list,
						IsAdmin : isAdmin,
						loginStatus : isLogin
					});
				});
		});	
	});
};

exports.postNotice = function(req, res){
	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		console.log("Login: " + req.signedCookies.userid + " admin: " +  isAdmin);

		if(isLogin && isAdmin){
			res.render( 'post_notice',{IsAdmin : true,loginStatus : true});
		}else{
			console.log("user is not an admin")
			return res.redirect('index');
		}
	});
};
exports.submitNotice = function(req,res){
	Notice.newNotice(
		req,
		res,
		mysql,
		function(){console.log('hi');return res.redirect('post_notice');},
		function(){
			res.render( 'submit_notice',{
				submit_status : '成功',
				post_message : '成功新增公告',
				return_path : 'post_notice',
				view_notice : 'notice?page=1',
				IsAdmin : true,
				loginStatus : true
			});
		}
	);
};
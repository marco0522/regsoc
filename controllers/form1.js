var mysql = require('../db');
var Form1  = require('../models/form1');
var CheckLogin  = require('../models/checkLogin');
var Upload_File  = require('../models/uploadFile');


exports.Enterform1 = function(req, res){
	console.log(req.signedCookies.userid);
	var isAdmin = CheckLogin.IsAdmin(req,res);
	var isLogin = CheckLogin.checkLoginStatus(req,res);
	Form1.Form1Init(req,res,function(name,type){
		res.render('form1',{
			SocName : name,
			SocType : type,
			IsAdmin : isAdmin,
			loginStatus : isLogin
		});
	});
};


exports.submit_form1 =  function(req, res) {
	//upload file
	var isAdmin = CheckLogin.IsAdmin(req,res);
	var isLogin = CheckLogin.checkLoginStatus(req,res);

	var total_file=2;
	var formNameList = [];

	formNameList[1] = 'constitution_url';
	formNameList[2] = 'member_list_url';
	Upload_File.uploadFile(req,res,formNameList,'./public/files/',1,total_file,'/form1',false);
	//other data
	//req.signedCookies.userid
	Form1.submitForm1(req,res,function(success){
		if(success){
			res.render( 'submit_form1',{
				upload_status : '成功',
				upload_message : '成功更新資料',
				return_path : 'form1',
				IsAdmin : isAdmin,
				loginStatus : isLogin
			});
		}else{
			return res.redirect( 'form1');
		}
	});
};
var mysql = require('../db');
var Form2 = require('../models/form2');
var CheckLogin = require('../models/checkLogin');


exports.Enterform2 = function(req, res){
	var isAdmin = CheckLogin.IsAdmin(req,res);
	var isLogin = CheckLogin.checkLoginStatus(req,res);
	if(!isLogin){
		console.log('not login');
		return res.redirect('login');
	}
	res.render('form2',{
		IsAdmin : isAdmin,
		loginStatus : isLogin
	});
};


exports.submit_form2 = function(req, res) {
	Form2.submitForm2(req,res,function(success,number){
		if(success){
			var isAdmin = CheckLogin.IsAdmin(req,res);
			var isLogin = CheckLogin.checkLoginStatus(req,res);
			res.render( 'submit_form2',{
				submit_status : '成功',
				post_message : '成功新增 ' + number+' 位幹事會成員',
				return_path : 'form2',
				IsAdmin : isAdmin,
				loginStatus : isLogin
			});
		}else{
			return res.redirect( 'form2');
		}
	});
};
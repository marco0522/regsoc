var isLogin = false;
var isAdmin = false;
var whichFrom = null;
var repotType = null;
var formNameList = [];
var whichForminFrom3 = null;
var AdminList = [];
var loginError = 0; //0->success , 1->wrong password , 2->no this user
var total_file = 0;

var mysql = require('../db');
var Upload_File  = require('../models/uploadFile');

var r_database = require('../models/database');

mysql.connect(function(err){
	if(!err){
		console.log("Database is conneccted...");
	}else{
		console.log("Error connecting databases");
	}
});

function whoIsAdmin(){
	var admin = [];
	mysql.query('select email,soc_code from User',function(err,rows,fields){
		if(err){
			console.log('error');
		}else{
			for(var i in rows){
				if(rows[i].soc_code=='admin'){
					admin.push(rows[i].email);
				}
			}
		}
		AdminList = admin;
	});
}
whoIsAdmin(); //list all the admin and store to AdminList


var checkLoginStatus = function(req, res){
	isLogin = false;
	isAdmin = false;
	if(req.signedCookies.userid){
		isLogin = true;
	}
	for(var i in AdminList){
		if(req.signedCookies.userid==AdminList[i]){
			isAdmin = true;
			break;
		}
	}
	console.log('isAdmin: '+isAdmin);
	// if(req.signedCookies.userid && req.signedCookies.password){
	// 	isLogin = true;
	// }
};
// function uploadFile (req,res,upload_field,target_des,current_file,total_file,return_to,show_page){

// 	if(upload_field!=undefined){//upload_field==undefined when there is no more new field for file uploading in that form
// moved to ../models/uploadFile
// };




//首頁
exports.index = function(req, res){
	checkLoginStatus(req,res);
	if(!isLogin){
		res.render('login',{
			IsAdmin : isAdmin,
			loginStatus : isLogin,
			loginErrorCode : loginError
		});
	}else{
		res.render('index',{
			IsAdmin : isAdmin,
			loginStatus : isLogin
		});
	}
	loginError = 0;
};

exports.login = function(req, res){
	checkLoginStatus(req,res);
	console.log('Login: username?:'+req.signedCookies.userid);
	res.render( 'login',{
		IsAdmin : isAdmin,
		loginStatus : isLogin,
		loginErrorCode : loginError
	} );	
};

//執行登入
exports.doLogin = function(req, res){
	mysql.query('select password,soc_code from User where email="'+req.body['user_id']+'"',function(err,rows,fields){
		if(err){
			console.log('error');
			return res.redirect('/');
		}else{
			if(rows.length==0){
				loginError = 2;
				console.log('Do not have this user');
				return res.redirect('/');
			}else if(rows[0].password!=req.body['password']){
				loginError = 1;
				console.log('Wrong Password');
				return res.redirect('/');
			}else if(rows[0].password==req.body['password']){
				console.log('logged in');
				res.cookie('userid', req.body['user_id'], { path: '/', signed: true});
				res.cookie('password', req.body['password'], { path: '/', signed: true });
				if(rows[0].soc_code=='admin'){
					console.log('admin');
				}else{
					console.log('normal user');
				}
				loginError = 0;
				return res.redirect('index');
			}
		}
	});
};

exports.logout = function(req,res){
	res.clearCookie('userid', { path: '/' });
	res.clearCookie('password', { path: '/' });

	return res.redirect('/');
};

exports.regsoc = function(req, res){
	checkLoginStatus(req,res);
	console.log('logged in > '+isLogin);
	if(!isLogin){
		console.log('not login');
		return res.redirect('login');
	}else{
		console.log('login');
		res.render( 'regsoc',{
			IsAdmin : isAdmin,
			loginStatus : isLogin
		});
	}
};

exports.notice = function(req, res){
//removed, please use controllers/notice.js 
};


exports.socinfo = function(req, res){
	checkLoginStatus(req,res);
	if(!isLogin){
		return res.redirect('login');
	}else{
		console.log('socinfo');
		res.render( 'socinfo',{
			IsAdmin : isAdmin,
			loginStatus : isLogin
		});
	}
};

exports.post_notice = function(req, res){
//removed
};

exports.submit_notice = function(req,res){
//removed
};


exports.regsoc1 = function(req, res){
	checkLoginStatus(req,res);
	res.render( 'regsoc1',{
		IsAdmin : isAdmin,
		loginStatus : isLogin
	});
};




exports.form3 = function(req, res){
	checkLoginStatus(req, res);
	if(!isLogin){
		console.log('not login');
		return res.redirect('login');
	}
	switch(req.query.form3){
		case 'A':
			whichFrom = "A - 去屆工作報告";
			repotType = "report";
			whichForminFrom3 = '/form3?form3=A';
			break;
		case 'B':
			whichFrom = "B - 去屆財政報告";
			repotType = "financialReport";
			whichForminFrom3 = '/form3?form3=B';
			break;
		case 'C':
			whichFrom = "C - 現屆工作計劃";
			repotType = "yearPlan";
			whichForminFrom3 = '/form3?form3=C';
			break;
		case 'D':
			whichFrom = "D - 現屆財政預算";
			repotType = "budget";
			whichForminFrom3 = '/form3?form3=D';
			break;
		default:
			res.write('Error - Form 3_ is missing');
			break;
	}
	res.render('form3',{
		IsAdmin : isAdmin,
		loginStatus : isLogin,
		whichFrom : whichFrom,
		repotType : repotType
	});
};

exports.form4 = function(req, res){
	checkLoginStatus(req,res);
	if(!isLogin){
		console.log('not login');
		return res.redirect('login');
	}
	res.render('form4',{
		IsAdmin : isAdmin,
		loginStatus : isLogin
	});
};


exports.submit_form3 =  function(req, res) {
	total_file=1;
	formNameList[1] = 'all_report_url';
	Upload_File.uploadFile(req,res,formNameList,'./public/files/',1,total_file,whichForminFrom3,true);
};
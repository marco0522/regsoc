-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: localhost    Database: regSocNet
-- ------------------------------------------------------
-- Server version	5.6.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AGM_Motion`
--

DROP TABLE IF EXISTS `AGM_Motion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AGM_Motion` (
  `agm_ref_no` int(11) NOT NULL AUTO_INCREMENT,
  `datatime` datetime NOT NULL,
  `location` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `chairman_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `chairman_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`agm_ref_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AGM_Motion`
--

LOCK TABLES `AGM_Motion` WRITE;
/*!40000 ALTER TABLE `AGM_Motion` DISABLE KEYS */;
/*!40000 ALTER TABLE `AGM_Motion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Exco_Info`
--

DROP TABLE IF EXISTS `Exco_Info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Exco_Info` (
  `exco_id` int(11) NOT NULL AUTO_INCREMENT,
  `year` year(4) DEFAULT NULL,
  `exco_name` varchar(255) NOT NULL,
  `exco_start_date` date DEFAULT NULL,
  `exco_end_date` date DEFAULT NULL,
  PRIMARY KEY (`exco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Exco_Info`
--

LOCK TABLES `Exco_Info` WRITE;
/*!40000 ALTER TABLE `Exco_Info` DISABLE KEYS */;
/*!40000 ALTER TABLE `Exco_Info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Exco_Member`
--

DROP TABLE IF EXISTS `Exco_Member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Exco_Member` (
  `exco_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `exco_id` int(11) DEFAULT NULL,
  `exco_member_post` varchar(50) DEFAULT NULL,
  `exco_member_Cname` varchar(30) DEFAULT NULL,
  `exco_member_Ename` varchar(50) DEFAULT NULL,
  `exco_member_sid` varchar(10) DEFAULT NULL,
  `exco_member_college` varchar(20) DEFAULT NULL,
  `exco_member_major` varchar(50) DEFAULT NULL,
  `exco_member_year_of_study` varchar(1) DEFAULT NULL,
  `exco_member_phone` varchar(8) DEFAULT NULL,
  `exco_member_email` varchar(50) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`exco_member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Exco_Member`
--

LOCK TABLES `Exco_Member` WRITE;
/*!40000 ALTER TABLE `Exco_Member` DISABLE KEYS */;
INSERT INTO `Exco_Member` VALUES (10,NULL,'早力','昨存穿','asdkjh asd aksd','123123','CC','ENG','1','23324',NULL,'2015-03-16 22:00:58');
/*!40000 ALTER TABLE `Exco_Member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Motion`
--

DROP TABLE IF EXISTS `Motion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Motion` (
  `ref_no` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `no_of_member` int(11) NOT NULL,
  `legal_mini_no` int(11) NOT NULL,
  `no_of_vote` int(11) NOT NULL,
  `no_of_agr` int(11) NOT NULL,
  `no_of_opp` int(11) NOT NULL,
  `no_of_abs` int(11) NOT NULL,
  `no_on_inv` int(11) NOT NULL,
  `pass_method` varchar(255) NOT NULL,
  `pass_method_ref_no` int(11) NOT NULL,
  PRIMARY KEY (`ref_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Motion`
--

LOCK TABLES `Motion` WRITE;
/*!40000 ALTER TABLE `Motion` DISABLE KEYS */;
/*!40000 ALTER TABLE `Motion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Notice`
--

DROP TABLE IF EXISTS `Notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Notice` (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `notice_content` text,
  `post_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Notice`
--

LOCK TABLES `Notice` WRITE;
/*!40000 ALTER TABLE `Notice` DISABLE KEYS */;
INSERT INTO `Notice` VALUES (1,'Title 1','1 notice_content','2015-03-13 02:20:15','2015-03-15 16:03:15'),(2,'title 2','2','2015-03-13 02:20:19','0000-00-00 00:00:00'),(3,'title 3','3','2015-03-13 02:20:23','0000-00-00 00:00:00'),(4,'Title 4','4notice_content','2015-03-13 02:20:26','2015-03-15 16:03:25'),(5,'kjdsfhaskdjf','sdfiosdhkjvsnvsdb','2015-03-16 04:25:18','0000-00-00 00:00:00'),(6,'jdkfksdjfsds','sdfsjdhfksdj','2015-03-16 04:26:13','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `Notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reg_Soc_Doc`
--

DROP TABLE IF EXISTS `Reg_Soc_Doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Reg_Soc_Doc` (
  `record_id` int(11) NOT NULL,
  `soc_code` varchar(10) NOT NULL,
  `exco_elect_ref_no` int(11) DEFAULT NULL,
  `year_plan_ref_no` int(11) DEFAULT NULL,
  `budget_ref_no` int(11) DEFAULT NULL,
  `report_ref_no` int(11) DEFAULT NULL,
  `financial_report_ref_no` int(11) DEFAULT NULL,
  `year_plan_url` varchar(255) DEFAULT NULL,
  `budget_url` varchar(255) DEFAULT NULL,
  `report_url` varchar(255) DEFAULT NULL,
  `financial_url` varchar(255) DEFAULT NULL,
  `note_by_exco` text,
  `note_by_committee` text,
  `reg_soc_status` varchar(30) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `1st_process_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `2nd_process_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Reg_Soc_Doc`
--

LOCK TABLES `Reg_Soc_Doc` WRITE;
/*!40000 ALTER TABLE `Reg_Soc_Doc` DISABLE KEYS */;
INSERT INTO `Reg_Soc_Doc` VALUES (1,'1',NULL,NULL,NULL,NULL,NULL,'./public/files/tutorial1.pptx','./public/files/1,jpg.webarchive','./public/files/4 - Production.pdf','./public/files/tuto05.pptx',NULL,NULL,NULL,'2015-03-18 09:59:03','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `Reg_Soc_Doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Soc_Info`
--

DROP TABLE IF EXISTS `Soc_Info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Soc_Info` (
  `soc_code` varchar(10) NOT NULL,
  `soc_name` varchar(255) DEFAULT NULL,
  `constitution_ref_no` int(11) DEFAULT NULL,
  `soc_type` varchar(10) DEFAULT NULL,
  `note_by_committee` varchar(255) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `num_of_mem` int(11) DEFAULT NULL,
  `mem_list_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`soc_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Soc_Info`
--

LOCK TABLES `Soc_Info` WRITE;
/*!40000 ALTER TABLE `Soc_Info` DISABLE KEYS */;
INSERT INTO `Soc_Info` VALUES ('1','香港中文大學學生會代表會',NULL,'approved',NULL,'2015-03-17 16:36:24',NULL,NULL);
/*!40000 ALTER TABLE `Soc_Info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `user_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `soc_code` varchar(10) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'admin@gmail.com','abcd1234','admin'),(2,'user@gmail.com','abcd1234','1');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-18 20:29:58

//module dependencies
var express = require('express'); //從local取得express
var routes = require('./routes'); //等同於"./routes/index.js"，指定路徑返回內容，相當於MVC中的Controller
var http = require('http');
var path = require('path');
var app = express();

var connection = require('./db');

//預設port號 3000，所以執行的URL為 http://localhost:3000
app.set('port', process.env.PORT || 3000);


app.set('views', path.join(__dirname, 'views'));//設計頁面模板位置，在views子目錄下
app.set('view engine', 'ejs');//表明要使用的模板引擎(樣板引擎，Template Engine)是ejs
// app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.bodyParser());//解析client端請求，通常是透過POST發送的內容
app.use(express.cookieParser('123456789'));//記得設定key來傳遞資訊
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


// app.use(multer({ dest: './public/files/',
//  rename: function (fieldname, filename) {
//     return filename+Date.now();
//   },
// onFileUploadStart: function (file,req,res) {
//   console.log(file.originalname + ' is starting ...')
// },
// onFileUploadComplete: function (file,req,res) {
//   console.log(file.fieldname + ' uploaded to  ' + file.path)
//   done=true;
// }
// }));



// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

app.get('/', routes.index);
app.post('/', routes.doLogin);

app.get('/index', routes.index);
app.post('/index', routes.doLogin);

app.get('/login', routes.login);
app.post('/login', routes.doLogin);

app.get('/logout', routes.logout);

//app.get('/notice', routes.notice);

app.get('/regsoc', routes.regsoc);

app.get('/socinfo',routes.socinfo);



app.get('/regsoc1',routes.regsoc1);

var form1 = require('./controllers/form1');
var form2 = require('./controllers/form2');

app.get('/form1',form1.Enterform1);
app.get('/form2',form2.Enterform2);
app.get('/form3',routes.form3);
app.get('/form4',routes.form4);

//forms upload
app.post('/submit_form1',form1.submit_form1);
app.post('/submit_form2',form2.submit_form2);
app.post('/submit_form3',routes.submit_form3);

var notice = require('./controllers/notice');
app.get('/notice', notice.getPost);
app.get('/post_notice',notice.postNotice);
app.post('/submit_notice',notice.submitNotice);


http.createServer(app).listen(app.get('port'), function( req, res ){ 
	console.log('Express server listening on port ' + app.get('port'));
});


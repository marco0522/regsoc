var mysql = require('../db');
var fs = require('fs');
var file_detail = {};
var totalFile = 0;
var Upload_File  = require('./uploadFile');
var CheckLogin  = require('./checkLogin');
var UFDB  = require('./uploadFileToDB');


exports.uploadFile = function (req,res,upload_field,target_des,current_file,total_file,return_to,show_page){
	if(upload_field[current_file]!=undefined){//upload_field==undefined when there is no more new field for file uploading in that form
		var url = req.files[upload_field[current_file]];
		if(url.type=='application/pdf'){//only accept pdf file?
			var tmpPath = url.path;
			console.log();
			var targetPath = target_des + url.name; //file location

			 // = {'hasFile':false,'FileName':null};

		    fs.rename(tmpPath, targetPath, function(err) {
		        if(err){
		        	console.log('current_file_1: '+current_file);
		        	current_file++;
		        	Upload_File.uploadFile(req,res,upload_field,target_des,current_file,total_file,return_to,show_page);
		        }else{
			        fs.unlink(tmpPath, function() {
			           	console.log('File Uploaded to ' + targetPath + ' - ' + url.size + ' bytes');
			           	//save file location to database.
			           	var whichcolumn = null;
			           	console.log('return: '+return_to+' whichcolumn: '+whichcolumn);
			           	switch(return_to){//handled form 3 only
	           				case '/form3?form3=A':
								whichcolumn = "report_url";
								break;
							case '/form3?form3=B':
								whichcolumn = "financial_url";
								break;
							case '/form3?form3=C':
								whichcolumn = "year_plan_url";
								break;
							case '/form3?form3=D':
								whichcolumn = "budget_url";
								break;
			           	}
		           		UFDB.uploadFileToDB(req,res,targetPath,whichcolumn);
			        });
			        file_detail[upload_field[current_file]] = url.name;
			        console.log(file_detail);
			        current_file++;
				    Upload_File.uploadFile(req,res,upload_field,target_des,current_file,total_file,return_to,show_page);
			    	
		        }
		    });
		}else{
			return res.redirect(return_to);
		}
	}else{
		var fileUploaded = [];
		for(var i in file_detail){
			fileUploaded.push(file_detail[i]);
		}
		fileUploaded = fileUploaded.join(' & ');
		console.log('file_deail:' + file_detail[upload_field[current_file]]);
		//console.log('fileUploaded:'+fileUploaded.length);
		//reset for next time upload
		file_detail={};   
		if(show_page){
	    	if(fileUploaded==''){
		    	return res.redirect(return_to);
		    }else{
		    	var isAdmin = CheckLogin.IsAdmin(req,res);
				var isLogin = CheckLogin.checkLoginStatus(req,res);
		    	res.render('submit_form1',{
					IsAdmin : isAdmin,
					loginStatus : isLogin,
					upload_status : '上傳成功',
					return_path : return_to,
		    		upload_message : fileUploaded + ' 已經上傳成功!'
		    	});
		    	//reset json for next time upload
		    	console.log('here');
		    }
		}else{

		} 
	}
};

